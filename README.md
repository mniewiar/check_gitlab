# check_gitlab

Gitlab Naemon/Icinga/Nagios plugin which checks various stuff via Gitlab API(v4) and output of gitlab-ctl services.

Tested with: Naemon 1.0.5, 1.0.6, 1.0.10; Ruby 2.3.0, 2.3.3

# Requirements
* Ruby >2.3

# Usage
```shell_session
./check_gitlab.rb
check_gitlab v0.4.0 [https://gitlab.com/6uellerBpanda/check_gitlab]

This plugin checks various parameters of Gitlab

Mode:
  health                    Check the Gitlab web endpoint for health
  services                  Check if any service of 'gitlab-ctl status' is down
  group_size                Check size of group in MB
  ci-pipeline               Check duration of a CI pipeline
  ci-runner                 Check status of CI runners
  ci-runner-jobs-duration   Check duration (in seconds) of running jobs of CI runners
  license-expires           Check remaining days when license expires - only warning status possible
  license-overage           Check if more active then licensed users are present

Usage: check_gitlab.rb [options]

Options:
    -s, --address ADDRESS            Gitlab address
    -t, --token TOKEN                Access token
    -i, --id ID                      Project/Group/CI-Runner ID
    -k, --insecure                   No ssl verification
    -m, --mode MODE                  Mode to check
    -n, --name NAME                  Name of group
    -e, --exclude EXCLUDE            Exclude group
    -w, --warning WARNING            Warning threshold
    -c, --critical CRITICAL          Critical threshold
    -d, --debug                      Print extra debugging/status output (available for health check)
    -v, --version                    Print version information
    -h, --help                       Show this help message
```

## Options
* -s: gitlab url, only https supported, https://gitlab.example.com

* -k: if you've a self signed cert

* -t: access token, required for all api calls except health mode

* -i: project id, group name, ci-runner id

* -n: name of the group

* -e: exclude a group

* -d: debug/verbose output. at the moment only possible with _health_ mode

# Modes
## Health check
Checks the status of the readiness endpoint.
See https://docs.gitlab.com/ce/user/admin_area/monitoring/health_check.html for more information.

Optional debug/verbose output possible.
```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m health -s <gitlab_url> [-k]
OK - Gitlab probes are in healthy state
```

## Services
If any service of 'gitlab-ctl status' reports down service status will be critical.

Requires a sudo entry to get output of gitlab-ctl command.

```conf
naemon ALL = NOPASSWD: /usr/bin/gitlab-ctl status
```

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m services
Critical - logrotate, mattermost is down
```

## CI Pipeline
Checks duration of finished CI pipeline in seconds with perfdata. ([/projects/:id/pipelines](https://docs.gitlab.com/ee/api/pipelines.html#list-project-pipelines))

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m ci-pipeline -s <gitlab_url> [-k] -t <access_token> -i <procject_id> -w 100 -c 200
Critical - Pipeline #265 took 265s | duration=265s;100;150
```

## CI Runner
Checks status of CI runner. ([/runners/all](https://docs.gitlab.com/ee/api/runners.html#list-all-runners))

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m ci-runner -s <gitlab_url> [-k] -t <access_token> -i <procject_id> -w 0 -c 2
Warning - 15 runner active, 1 runner not active | unactive_runners=1;0;1
```

## CI Runner jobs duration
Checks duration (in seconds) of the first found running job with perfdata. ([/runners/:id/jobs](https://docs.gitlab.com/ee/api/runners.html#list-runner-s-jobs))

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m ci-runner-jobs-duration -s <gitlab_url> [-k] -t <access_token> -i <ci_runner_id> -w 5 -c 10
Critical - ansible_lint is running for 15s | duration=15s;5;10
```

## Group size
Checks size of a group in megabytes with perfdata. ([/groups](https://docs.gitlab.com/ee/api/groups.html#list-groups))

Authenticated user/token needs admin rights.

Search can produce more then one result if name overlaps. Limit it via the exclude option - only one group possible. In general it will use the first found group which matches.

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m group_size -s <gitlab_url> [-k] -t <access_token> -n <group_name> -w 3000 -c 4000
Warning - Utils: used space 3251MB | Utils=3251MB;3000;4000
```

## License
For license checks authenticated user/token needs admin rights.

### Expire
Check remaining days when license expires. Specify less then remaining days via warning threshold. ([/license](https://docs.gitlab.com/ee/api/license.html))

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m license-expire -s <gitlab_url> [-k] -t <access_token> -w 40
Warning - License  will expire at 2019-08-22 - 51 days left
```

### Overage
Check for over-subscription. ([/license](https://docs.gitlab.com/ee/api/license.html))

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m license-overage -s <gitlab_url> [-k] -t <access_token> -w 1 -c 5
OK - Active users: 43, Overage: 0 | active_users=43 overage=0;1;5

```
